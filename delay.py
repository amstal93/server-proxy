from time import sleep
from typing import Callable


def delay(second_to_wait: Callable[[], float] = lambda: 1.0):
    def decorated(func):
        def wrapper(*args, **kwargs):
            result = func(*args, **kwargs)
            sleep(second_to_wait())
            return result

        return wrapper

    return decorated
