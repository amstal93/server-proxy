from re import compile as re_compile


request_headers_black_list = (
    'Accept-Encoding',
    'Host',
)
response_headers_black_list = (
    'Transfer-Encoding',
)
path_pattern = re_compile(r'/[a-z0-9_/&;-]+')
secure_pattern = re_compile(r'Secure')


def _format_request_data(request):
    if request.data:
        request_data = request.data
    elif request.form:
        form_data = [
            key + '=' + value
                for (key,value) in request.form.items()
        ]
        form_data = '&'.join(form_data)
        request_data = form_data.encode('utf-8')
    else:
        request_data = None

    return request_data


def _sanitize_request_headers(headers):
    headers_sanitized = dict(
        (key,value)
            for (key,value) in headers
                if key not in request_headers_black_list
    )

    return headers_sanitized


def _sanitize_response_headers(headers):
    headers_sanitized = dict(
        (key,value)
            for (key,value) in headers
                if key not in response_headers_black_list
    )

    if 'Set-Cookie' in headers_sanitized:
        headers_sanitized['Set-Cookie'] = path_pattern.sub('/', headers_sanitized['Set-Cookie'])
        headers_sanitized['Set-Cookie'] = secure_pattern.sub('', headers_sanitized['Set-Cookie'])

    return headers_sanitized
