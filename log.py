from flask import (
    make_response,
    request,
)
from pprint import pformat
from .sanitize import (
    _format_request_data,
    _sanitize_request_headers,
    _sanitize_response_headers,
)


def log(real_server_url: str):
    def log_decorated(func):
        def log_wrapper(*args, **kwargs):
            raw_response = func(*args, **kwargs)
            response = make_response(raw_response)

            method = request.method
            status = '%s %s' % (response.status_code, response.status)
            url = real_server_url + request.full_path

            request_headers = _sanitize_request_headers(request.headers.items())
            request_data = _format_request_data(request)

            response_headers = _sanitize_response_headers(response.headers.items())
            response_data = response.get_json(force=True, silent=True) or response.data

            print('\n'.join([
                '%s %s %s' % (method, status, url),
                'request headers %s' % pformat(request_headers),
                'request body %s' % pformat(request_data),
                'response headers %s' % pformat(response_headers),
                'response body %s' % pformat(response_data),
            ]))

            return response

        return log_wrapper

    return log_decorated
