# Server Proxy

Small utility that helps to tweak HTTP server using a [Flask](https://flask.palletsprojects.com/en/1.1.x/) server

## Schemas

### Normal usage

```plantuml
@startuml "Normal usage"
agent "Client" as client
agent "Real server" as server

client -> server
@enduml
```

### Usage with proxy

```plantuml
@startuml "Usage with proxy"
agent "Client" as client
agent proxy [
	Proxy server
	====
	You can tweak it
	There is hot reload
]
agent "Real server" as server

client -> proxy
proxy -> server
@enduml
```

## Usage

1. Install [Docker](https://docs.docker.com/get-docker/)
1. Install [Docker Compose](https://docs.docker.com/compose/install/)
1. Clone [this repository](https://gitlab.com/pinage404/server-proxy)

   ```shell
   git clone https://gitlab.com/pinage404/server-proxy
   ```

1. Update [`REAL_SERVER_URL` in `docker-compose.yml`](https://gitlab.com/pinage404/server-proxy/-/blob/main/docker-compose.yml#L11)
1. Change the URL of your API in your client to `http://localhost:5000` (by default)
1. Start server proxy

   ```shell
   docker-compose up
   ```

## Features

Whatever you will do by adding a route using the [Flask API](https://flask.palletsprojects.com/en/1.1.x/api/#url-route-registrations) or tweaking the current code

### Pass request to the real server

This act same as without this proxy

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Log full request and response

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@log(REAL_SERVER_URL)
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### In memory cache response

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@cache(REAL_SERVER_URL)
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Delay response

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@delay()
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

#### Random delay

```python
@app.route('/', methods=MAIN_HTTP_METHODS)
@app.route('/<path:path>', methods=MAIN_HTTP_METHODS)
@delay(lambda: rand(0.0, 3.0))
@pass_to_server(REAL_SERVER_URL)
def proxy_to_server():
    pass
```

### Fake response

```python
@app.route('/api/user', methods=['GET'])
def user():
    return jsonify({
        'name': 'foo',
        'id': 404,
    })
```

### Multi servers support

Add [`service`](https://docs.docker.com/compose/compose-file/compose-file-v3/#service-configuration-reference) in the [`docker-compose.yml`](https://gitlab.com/pinage404/server-proxy/-/blob/main/docker-compose.yml)

Exemple:

```yaml
version: '3'

services:
    user_api:
        build: .
        volumes:
            - $PWD:/srv/server-proxy
        ports:
            - "5000:5000"
        environment:
            REAL_SERVER_URL: 'https://example.com/api/user/v2/'
            PORT: 5000
            FLASK_APP: server-proxy.py
            FLASK_DEBUG: 1
            FLASK_ENV: development
        network_mode: host
        tty: true
        command: sh run.sh

    document_api:
        build: .
        volumes:
            - $PWD:/srv/server-proxy
        ports:
            - "6000:6000"
        environment:
            REAL_SERVER_URL: 'https://example.com/api/document/v1/'
            PORT: 6000
            FLASK_APP: server-proxy.py
            FLASK_DEBUG: 1
            FLASK_ENV: development
        network_mode: host
        tty: true
        command: sh run.sh
```

In your client configuration change the API's URL

```diff
-user-api: https://example.com/api/user/v2/
+user-api: http://localhost:5000
-document-api: https://example.com/api/document/v1/
+document-api: http://localhost:6000
```

## License

[ISC License](./LICENSE)
